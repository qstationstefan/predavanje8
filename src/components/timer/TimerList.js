import { useState, useRef } from "react"
import Timer from "./Timer";

const TimerList = () => {
    // const idAutoIncrement = useRef(0);
    // const [timers, setTimers] = useState([]);

    // const addTimerHandler = () => {
    //     setTimers(prevState => [...prevState, <Timer key={idAutoIncrement.current++}/>])
    // }

    const idAutoIncrement = useRef(0);
    const [timers, setTimers] = useState([]);

    const addTimerHandler = () => {
        idAutoIncrement.current++;
        setTimers(prevState => [...prevState, idAutoIncrement.current])
    }

    // console.log(timers);
    return (
        <div>
            <h1>Timers:</h1>
            <button onClick={addTimerHandler}>Add new timer</button>
            {timers.map(timerId => <Timer key={timerId} />)}
            {/* {timers} */}
        </div>
    )
}

export default TimerList;