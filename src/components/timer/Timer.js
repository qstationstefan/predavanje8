import { useState, useEffect, useRef } from "react";

const Timer = () => {
    const [timerValue, setTimerValue] = useState(0);
    const [isRunning, setIsRunning] = useState(false);

    useEffect(() => {
        let id;
        if (isRunning) {
            id = setInterval(() => {
                    setTimerValue(prevState => prevState + 1);
            }, 10);
        }

        return () => {
            if (id) {
                clearInterval(id);
            }
        }
    }, [isRunning]);

    const toggleTimerHandler = () => {
        setIsRunning(prevState => !prevState);
    }

    return (
        <div>
            <p>{`Time: ${(timerValue / 100)}s`}</p>
            <button onClick={toggleTimerHandler}>Start/Stop timer</button>
        </div>
    )
}

export default Timer;